module.exports = function UserControlled(
    initial,
    action
) {
    var setValue = initial;
    var lastUserSet = new Date();

    var overrideTimeMilis = 5000; // 5 sec

    this.userSet = function (value) {
        setValue = value;
        lastUserSet = new Date();
        action(value)
    };

    this.current = function (value) {
        if (lastUserSet.getTime() + overrideTimeMilis < new Date().getTime()) {
            setValue = value;
        } else if (setValue != value) {
            action(setValue)
        }
    };

    this.get = function () {
        return setValue;
    };
}