var execSync = require('child_process').execSync;
var UserControlled = require('./user-controlled');

module.exports = function Chromecast(
    globals,
    name
) {
    var Service = globals.Service;
    var Characteristic = globals.Characteristic;

    var ip = '192.168.0.2'

    var unmuted = new UserControlled(true, function (value) {
        if (value) {
            console.log("Unmuting Chromecast");
            execSync('chromecast -H ' + ip + ' unmute');
        } else {
            console.log("Muting Chromecast");
            execSync('chromecast -H ' + ip + ' mute');
        }

    });
    var volume = new UserControlled(100, function (value) {
        console.log("Changing Chromecast volume");
        execSync('chromecast -H ' + ip + ' volume ' + value / 100);
    });

    this.name = name;

    var levelService = new Service.Lightbulb(name);
    levelService
        .getCharacteristic(Characteristic.On)
        .on('get', function (callback) {
            console.log("Chromecast.Unmute.get() = " + unmuted.get());
            return callback(null, unmuted.get());
        })
        .on('set', function (newState, callback) {
            console.log("Chromecast.Unmute.set(" + newState + ")");
            unmuted.userSet(newState);
            return callback(null);
        });
    levelService
        .getCharacteristic(Characteristic.Brightness)
        .on('get', function (callback) {
            console.log("Chromecast.Volume.get() = " + volume.get());
            return callback(null, volume.get());
        })
        .on('set', function (newVolume, callback) {
            console.log("Chromecast.Volume.set(" + newVolume + ")");
            volume.userSet(newVolume);
            return callback(null);
        });

    this.accessory = function(log, config) {
        console.log("Chromecast accessory")
        this.getServices = function () {
            return [levelService];
        }
    }

    this.run = function () {
        setInterval(function () {
            var command = 'chromecast -H ' + ip + ' status'
            try {
                var output = execSync(command)
                var volumeValue = /level: ([^,]+)/g.exec(output)[1];
                var mutedValue = /muted: ([^,]+)/g.exec(output)[1];

                if (mutedValue == 'true') {
                    unmuted.current(false);
                } else if (mutedValue == 'false') {
                    unmuted.current(true);
                }
                volume.current(100 * parseFloat(volumeValue));
            } catch (e) {
                console.log("Failed to update chromecast status: " + e.toString());
            }
        }, 1000);
    };
}