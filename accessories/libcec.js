var execSync = require('child_process').execSync;
var UserControlled = require('./user-controlled');

module.exports = function Libcec(
    globals,
    name
) {
    var Service = globals.Service;
    var Characteristic = globals.Characteristic;

    var turnedOn = new UserControlled(false, function (value) {
        if (value) {
            console.log("turning on TV");
            execSync('cec-client -s -d 1', { input: 'on 0' });
        } else {
            console.log("turning off TV");
            execSync('cec-client -s -d 1', { input: 'standby 0' });
        }
    });

    this.name = name;

    var switchService = new Service.Switch(name)
    switchService
        .getCharacteristic(Characteristic.On)
        .on('get', function (callback) {
            console.log("Libcec.get() = " + turnedOn.get());
            return callback(null, turnedOn.get());
        })
        .on('set', function (newState, callback) {
            console.log("Libcec.set(" + newState + ")");
            turnedOn.userSet(newState);
            return callback(null);
        });

    this.accessory = function(log, config) {
        console.log("TV accessory")

        this.getServices = function () {
            return [switchService];
        };
    };

    this.run = function () {
        setInterval(function () {
            try {
                var powerState = execSync('cec-client -s -d 1', { input: 'pow 0' })
                if (powerState.includes('power status: on')) {
                    turnedOn.current(true);
                } else if(powerState.includes('power status: standby')) {
                    turnedOn.current(false);
                }
            } catch (e) {
                console.log("Failed to update TV status: " + e.toString());
            }
        }, 1000);
    };
}