var HomebridgeGlobals = require("./homebridge-globals")
var Libcec = require("./accessories/libcec")
var Chromecast = require("./accessories/chromecast")

var homebridgeGlobals, Accessory, UUIDGen;

var pluginName = "homebridge-plugin-3715"

module.exports = function (homebridge) {
    console.log("homebridge API version: " + homebridge.version);

    homebridgeGlobals = new HomebridgeGlobals(
        homebridge.hap.Service,
        homebridge.hap.Characteristic
    );

    var libcec = new Libcec(homebridgeGlobals, "TV");
    libcec.run();
    homebridge.registerAccessory(pluginName, libcec.name, libcec.accessory);

    var chromecast = new Chromecast(homebridgeGlobals, "Chromecast");
    chromecast.run();
    homebridge.registerAccessory(pluginName, chromecast.name, chromecast.accessory);
};