module.exports = function HomebridgeGlobals(
    Service,
    Characteristic,
) {
    this.Service = Service;
    this.Characteristic = Characteristic;
};